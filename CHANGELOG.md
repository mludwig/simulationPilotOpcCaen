# Change Log
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [devel]
### Added

### Changed

### Fixed

## [0.9.7] - 11 sep 2018
### Added
- OPC-UA Venus Pilot server
- pilot server mutex protection against async user zmq messages
- proper noise level control for VMon and IMon 
- VenusCombo config files in a separate directory

### Changed
- suppressed old pilotUI from the combo for now (must update before)
- only .open6 version available for OPC-UA Caen and OPC-UA pilot

### Fixed
- mem hole: deallocation of messages: C++ needs explicit deallocation, C not