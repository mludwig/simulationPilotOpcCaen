README.md

simulationPilotOpcCaen: OPC UA server for pilot interface to venus simulation interface.
Functionality comes in two parts:
1. normal pilot interface to manage loads, s.engine behavior: AS0
2. scada test transmission counting: each AS node has a counter for each update, and these are published in AS1


initial steps to create a quasar project "venusCaenPilot" :
1. cd /home/mludwig/quasar-simulationPilotOpcCaen
2. git clone https://github.com/quasar-team/quasar.git
3. cd ./quasar
4. copy / create a build config and set it: ./quasar.py set_build_config ./pcbe13632_ua_build.cmake
5. try an empty build without project: ./quasar.py build (should succeed, if not, correct the build config)
6. ./quasar.py create_project venusCaenPilot
7. cd ./venusCaenPilot
8. cp ../pcbe13632_ua_build.cmake ./
9. ./quasar.py set_build_config ./pcbe13632_ua_build.cmake
10. ./quasar.py build (should succeed in building the still empty project in ./bin/OpcUaServer )
11. copy the whole directory /home/mludwig/quasar-simulationPilotOpcCaen/quasar/venusCaenPilot to gitlab/eclipse
12. create a device class VENUSCAENPILOT by editing ./Design.xml
13. ./quasar.py generate device VENUSCAENPILOT (interactive merging)
14. 


