#!/bin/bash
python ./quasar.py enable_module open62541_compat
python ./quasar.py set_build_config ./pcbe13632_open6_build.cmake
#
# the compat layer update seems to be broken. Clone it by hand
git clone https://github.com/quasar-team/open62541-compat.git
#python ./quasar.py prepare_build
python ./quasar.py build


