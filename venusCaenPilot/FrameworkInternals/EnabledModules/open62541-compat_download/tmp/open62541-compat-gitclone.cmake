if("master" STREQUAL "")
  message(FATAL_ERROR "Tag for git checkout should not be empty.")
endif()

set(run 0)

if("/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/open62541-compat-gitinfo.txt" IS_NEWER_THAN "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/open62541-compat-gitclone-lastrun.txt")
  set(run 1)
endif()

if(NOT run)
  message(STATUS "Avoiding repeated git clone, stamp file is up to date: '/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/open62541-compat-gitclone-lastrun.txt'")
  return()
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E remove_directory "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: '/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat'")
endif()

# try the clone 3 times incase there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
    COMMAND "/bin/git" clone "https://github.com/quasar-team/open62541-compat.git" "open62541-compat"
    WORKING_DIRECTORY "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot"
    RESULT_VARIABLE error_code
    )
  math(EXPR number_of_tries "${number_of_tries} + 1")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS "Had to git clone more than once:
          ${number_of_tries} times.")
endif()
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: 'https://github.com/quasar-team/open62541-compat.git'")
endif()

execute_process(
  COMMAND "/bin/git" checkout master
  WORKING_DIRECTORY "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: 'master'")
endif()

execute_process(
  COMMAND "/bin/git" submodule init
  WORKING_DIRECTORY "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to init submodules in: '/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat'")
endif()

execute_process(
  COMMAND "/bin/git" submodule update --recursive
  WORKING_DIRECTORY "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: '/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat'")
endif()

# Complete success, update the script-last-run stamp file:
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy
    "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/open62541-compat-gitinfo.txt"
    "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/open62541-compat-gitclone-lastrun.txt"
  WORKING_DIRECTORY "/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/open62541-compat"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to copy script-last-run stamp file: '/home/mludwig/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/open62541-compat-gitclone-lastrun.txt'")
endif()

