/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuasarServer.h"
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/foreach.hpp>
#include <LogIt.h>
#include <string.h>
#include <shutdown.h>

#include "../../Device/include/DVENUSCAENPILOT.h"
#include "../../Device/include/DGENERICPILOT.h"

#include "ThreadLauncher.h"
#include "Configurator.h"


QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
    	boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server for GENERICPILOT, reading configuration.";
	Device::DGENERICPILOT::readConfig();
	BOOST_FOREACH(Device::DGENERICPILOT* dev, Device::DRoot::getInstance()->genericpilots())
	{
		dev->init();
	}


    LOG(Log::INF) << "Initializing Quasar server for VENUSCAENPILOT, reading configuration.";
	Device::DVENUSCAENPILOT::readConfig();
	BOOST_FOREACH(Device::DVENUSCAENPILOT* dev, Device::DRoot::getInstance()->venuscaenpilots())
	{
		// have each device on its own thread: tell the thread which dev, and the device which thread.
		boost::shared_ptr<ThreadLauncher> threadlauncher(new ThreadLauncher());
		threadlauncher->setDVENUSCAENPILOT( dev );
		dev->setThreadLauncher( threadlauncher );
		LOG(Log::INF) << "starting thread for " << dev->getFullName();
		threadlauncher->start();
	}

}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
	Log::initializeLogging( Log::TRC );
	LOG(Log::INF) << "Logging initialized to TRC.";
}
