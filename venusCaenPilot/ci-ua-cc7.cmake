# @author mludwig
# Tue Jun  6 17:01:58 CEST 2017
# configure build chain for unified automation demo sdk (runs 30mins) and boost on local machine

message("using build configuration pcbe13632_ua_build.cmake")
SET ( PROTO_LIBS 
	/usr/lib64/libprotobuf-c.so 
	/usr/lib64/libprotobuf.so.8 
	/usr/lib64/libprotoc.so.8 )
	 
#	-lczmq  -ldl -llzma -lxml2 

SET ( SERVER_LINK_LIBRARIES -lpthread -lrt -lcrypto -lssl -lczmq  )


#------- 
#Boost
#-------
SET ( BOOST_HOME "/opt/3rdPartySoftware/boost/1.59.0/MODIFIED_NAMESPACE_INSTALL" )
SET( BOOST_PATH_HEADERS ${BOOST_HOME}/include )
SET( BOOST_PATH_LIBS ${BOOST_HOME}/lib )

message ( INFO "****** replaced BOOST with ${BOOST_HOME} **********" )

message(STATUS "BOOST - include [${BOOST_PATH_HEADERS}] libs [${BOOST_PATH_LIBS}] BOOST_LIB_SUFFIX [$ENV{BOOST_LIB_SUFFIX}]")
 
if(NOT TARGET libboostprogramoptions)
        add_library(libboostprogramoptions STATIC IMPORTED)
        set_property(TARGET libboostprogramoptions PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_program_options$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostsystem)
        add_library(libboostsystem STATIC IMPORTED)
        set_property(TARGET libboostsystem PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_system$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostfilesystem)
        add_library(libboostfilesystem STATIC IMPORTED)
        set_property(TARGET libboostfilesystem PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_filesystem$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostchrono) 
        add_library(libboostchrono STATIC IMPORTED)
        set_property(TARGET libboostchrono PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_chrono$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostdatetime) 
        add_library(libboostdatetime STATIC IMPORTED)
        set_property(TARGET libboostdatetime PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_date_time$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostthread) 
        add_library(libboostthread STATIC IMPORTED)
        set_property(TARGET libboostthread PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_thread$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostlog)
        add_library(libboostlog STATIC IMPORTED)
        set_property(TARGET libboostlog PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_log$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostlogsetup)
        add_library(libboostlogsetup STATIC IMPORTED)
        set_property(TARGET libboostlogsetup PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_log_setup$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostregex)
	add_library(libboostregex STATIC IMPORTED)
	set_property(TARGET libboostregex PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_regex$ENV{BOOST_LIB_SUFFIX}.a)
endif()


# set( BOOST_LIBS  libboostlogsetup libboostlog libboostsystem libboostfilesystem libboostthread libboostprogramoptions libboostchrono libboostdatetime)
set( BOOST_LIBS  libboostlogsetup libboostlog libboostsystem libboostfilesystem libboostthread libboostprogramoptions libboostchrono libboostdatetime libboostregex ${SERVER_LINK_LIBRARIES} )


# UA toolkit, commercial
add_definitions( -DBACKEND_UATOOLKIT )
SET ( OPCUA_TOOLKIT_PATH "/opt/3rdPartySoftware/UnifiedAutomation/1.5.5/sdk" )
# order is important: these libs depend on each other
SET ( OPCUA_TOOLKIT_LIBS "-luamodule -lcoremodule -lcoremoduled -luabase -luabased -luaclient -luaclientd -luamodels -luamodelsd  -luamoduled -luapki -luapkid -lxmlparser -lxmlparserd -luastackd -luastack ${SERVER_LINK_LIBRARIES}" )

SET ( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS} )
SET ( OPCUA_TOOLKIT_LIBS_DEBUG ${OPCUA_TOOLKIT_LIBS} )

# open6 toolkit
# add_definitions( -DBACKEND_OPEN62541 )
#set( OPEN62541_DEPENDENCIES -pthread -lssl -lcrypto -lrt )
#SET( OPCUA_TOOLKIT_PATH "" ) # open62541 toolkit is cloned from github and built via the open62541-compat #module (see open62541-compat/prepare.py)
#SET( OPCUA_TOOLKIT_LIBS_RELEASE "${PROJECT_SOURCE_DIR}/open62541-compat/open62541/build/libopen62541.so" ${OPEN62541_DEPENDENCIES})
#SET( OPCUA_TOOLKIT_LIBS_DEBUG "${PROJECT_SOURCE_DIR}/open62541-compat/open62541/build/libopen62541.so" ${OPEN62541_DEPENDENCIES})

#-----
# LogIt
#-----
SET( LOGIT_HAS_STDOUTLOG TRUE )
SET( LOGIT_HAS_BOOSTLOG TRUE )
SET( LOGIT_HAS_UATRACE FALSE )
MESSAGE( STATUS "LogIt build options: stdout [${LOGIT_HAS_STDOUTLOG}] boost [${LOGIT_HAS_BOOSTLOG}] uaTrace [${LOGIT_HAS_UATRACE}]" )

#-----
# XML Libs
#-----
#As of 03-Sep-2015 I see no FindXerces or whatever in our Cmake 2.8 installation, so no find_package can be user...
# TODO perhaps also take it from environment if requested
SET( XML_LIBS "-lxerces-c -lxml2" )

#-----
# GoogleTest
#-----
#include_directories( ${PROJECT_SOURCE_DIR}/GoogleTest/gtest/src/gtest/include )

#-----
# Compiler options (note: currently linux specific)
#-----
add_definitions(-Wall -Wno-deprecated -std=gnu++0x -Wno-literal-suffix -g)


