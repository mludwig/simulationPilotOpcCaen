set(CUSTOM_SERVER_MODULES )
set(EXECUTABLE OpcVenusCaenPilotServer )
set(SERVER_INCLUDE_DIRECTORIES  ThreadLauncher/include Proto/include )
SET ( PROTO_LIBS 
	/usr/lib64/libprotobuf-c.so 
	/usr/lib64/libprotobuf.so.8 
	/usr/lib64/libprotoc.so.8  )
set(SERVER_LINK_LIBRARIES ${PROTO_LIBS} )
set(SERVER_LINK_DIRECTORIES )
set(CUSTOM_SERVER_MODULES ThreadLauncher Proto)
