#ifndef THREADLAUNCHER_INCLUDE_THREADLAUNCHER_H_
#define THREADLAUNCHER_INCLUDE_THREADLAUNCHER_H_

#include <set>
#include <boost/thread.hpp>
#include <boost/thread/locks.hpp>

#include "../../Device/include/DVENUSCAENPILOT.h"

class DVENUSCAENPILOT;

class ThreadLauncher
{
public:
	ThreadLauncher();
	virtual ~ThreadLauncher();

	void start();
	void stop();
	void setDVENUSCAENPILOT( Device::DVENUSCAENPILOT *d ){ m_dvenuscaenpilot = d; }

	// needed for threads
	void operator()();
	static boost::mutex mtx;

private:
	Device::DVENUSCAENPILOT * m_dvenuscaenpilot;

	boost::thread m_thread;

	bool m_scheduleStopRunning;
	bool m_isRunning;
	uint32_t m_incrementPeriodMs;
};


#endif /* THREADLAUNCHER_INCLUDE_THREADLAUNCHER_H_ */
