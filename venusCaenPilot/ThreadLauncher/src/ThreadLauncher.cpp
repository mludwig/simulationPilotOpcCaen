/*
 * Incrementer.cpp
 *
 *  Created on: Oct 31, 2016
 *      Author: bfarnham
 */

#include "ThreadLauncher.h"
#include <set>
#include <boost/thread.hpp>
#include <LogIt.h>

using std::set;
/* static */ boost::mutex ThreadLauncher::mtx;


ThreadLauncher::ThreadLauncher(): m_scheduleStopRunning(false), m_isRunning(false), m_incrementPeriodMs(10000)
{
	m_dvenuscaenpilot = NULL;
}

ThreadLauncher::~ThreadLauncher()
{
	stop();
}

void ThreadLauncher::start()
{
	LOG(Log::INF) << __FUNCTION__ << " called, starting thread, current status running ? ["<<(m_isRunning?'Y':'N')<<"] stop scheduled ? ["<<(m_scheduleStopRunning?'Y':'N')<<"]";
	if(m_isRunning || m_scheduleStopRunning) return; // ignore.
	m_scheduleStopRunning = false;
	m_thread = boost::thread(boost::ref(*this));
}

void ThreadLauncher::stop()
{
	LOG(Log::INF) << __FUNCTION__ << " called, stopping increment thread, current status running ? ["<<(m_isRunning?'Y':'N')<<"] stop scheduled ? ["<<(m_scheduleStopRunning?'Y':'N')<<"]";
	if(!m_isRunning) return; // ignore
	if(m_scheduleStopRunning) return; // ignore

	m_scheduleStopRunning = true;
	m_thread.join();

	m_isRunning = false;
	m_scheduleStopRunning = false;
}



// used for main thread function, calls Device methods
void ThreadLauncher::operator()()
{
	LOG(Log::INF) << __FUNCTION__ << " init device " << m_dvenuscaenpilot->getFullName();
	m_dvenuscaenpilot->init();
	LOG(Log::INF) << __FUNCTION__ << " increment thread period ["<<m_incrementPeriodMs<<"ms] "<< m_dvenuscaenpilot->getFullName();
	m_isRunning = true;
	while(!m_scheduleStopRunning)
	{
		m_dvenuscaenpilot->update();
		boost::this_thread::sleep_for(boost::chrono::milliseconds(m_incrementPeriodMs));
	}
	LOG(Log::INF) << __FUNCTION__ << " end, OK";
}
