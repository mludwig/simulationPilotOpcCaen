
include_directories(
	include
)

# C sources, generated by the proto-c compiler from a google protocol specification
# we should get them fresh automatically
function ( copyProtoSources )
 	message("copying protobuf generated source and header")
 	SET(SRC_PATH "../../simulationOPCglue/protocolBuffers-c")
	SET(DEST_PATH "./Proto") 
  	execute_process(COMMAND cp -v ${SRC_PATH}/simulationGlueCaen.pb-c.c ${DEST_PATH}/src )
 	execute_process(COMMAND cp -v ${SRC_PATH}/simulationGlueCaen.pb-c.h ${DEST_PATH}/include )
	message("done, copying protobuf generated source and header")
endfunction()

copyProtoSources()

file(GLOB PROTO_SRCS src/*.c)
add_library (Proto OBJECT ${PROTO_SRCS})
add_dependencies (Proto LogIt Device)
